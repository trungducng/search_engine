/**
 * Created by trungducng on 12/03/2017.
 */

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var morgan = require('morgan');
var elasticsearch = require('elasticsearch');

var client = new elasticsearch.Client({
    host: 'localhost:9200',
    log: 'trace'
});

//middleware
app.use(morgan('common'));
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());

//view engine
app.set('view engine', 'ejs');
app.set('views', './views');

app.get('/add', (req, res) => {
    res.render('add');
});

app.post('/add', (req, res) => {
    if(req.body.add){
        if(req.body.title && req.body.author && req.body.content){
            let title = req.body.title;
            let author = req.body.author;
            let content = req.body.content;
            client.bulk({
                body:[
                    // action description
                    { index:  { _index: 'books', _type: 'book', _id: 1 } },
                    // the document to index
                    { title: title, author: author, content: content},
                ]
            }, (err) => {
                if(err) throw console.log(err);
                res.redirect('/search');
            });
        }
    }
});

app.get('/search', (req, res) => {
     res.render('search');
});

app.post('/search', (req, res) => {
    if(req.body.search){
        if(req.body.something){
            let title = req.body.something;
            let result = client.search({
                index: 'books',
                body: {
                    query: {
                        match: {
                            title: title
                        }
                    }
                }
            }, (err) => {
                if(err) throw console.log(err);
            });
            console.log(result);
        }
    }
});

app.listen(8080, () => {
    console.log('OK');
})